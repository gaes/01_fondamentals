# - Installar Chrome #
https://www.google.com/chrome/browser/thankyou.html?statcb=1&clickonceinstalled=1&installdataindex=defaultbrowser

# - Install Git#
https://git-scm.com/

http://www.winelove.club/doc/git/#/

# - Install Node.Js #
https://nodejs.org/es/download/

# - Install Angular 4 #
https://angular.io/guide/quickstart

npm install -g @angular/cli

# - Install Ionic 3
https://ionicframework.com/getting-started/

npm install -g ionic cordova

# - Check enviroment #
* java - version
* node -v
* npm -v
* ng -v
* ionic -v
* cordova -v

# - Test Angular4
* ng new TestAngular
* ng serve

# - Test Ionic3
* ionic start myNewProject tabs
* ionic serve

________________________________________________________________

# - Download Visual Code
https://code.visualstudio.com/


# - CRUD EXAMPLE #
https://www.youtube.com/watch?v=tv4gBwEx3hE